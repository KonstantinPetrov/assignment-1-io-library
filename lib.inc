section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall  

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0 
        je .end ;if byte at address rdi=0 => .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:

    push rdi
    call string_length;get string length
    pop rsi

    mov rdx, rax ;amount of output
    
    mov rax, 1
    mov rdi, 1
    

    syscall
    
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ;put on stek, becouse we need symbol number

    mov rdx, 1
    mov rsi, rsp
    

    mov rax, 1
    mov rdi, 1
    
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8,10
    mov r9,rsp
    push 0
    .loop:
        xor rdx, rdx
        div r8
        add rdx, '0'

        dec rsp
        mov byte [rsp],dl
        cmp rax,0
        je .end
        jmp .loop
    .end:
        mov rdi, rsp
        push r9
        call print_string
        pop rsp 
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi,0
    jl .negative
    jmp print_uint
    .negative:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r8, r8 
    .loop:
        mov r8b, byte[rdi+rcx]
        cmp r8b, byte[rsi+rcx]
        
        jne .no 
        cmp r8b,0
        je .yes 
        inc rcx
        jmp .loop
    
    .yes:
        mov rax, 1
        ret
    .no:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax 
    xor rdi,rdi 
    mov rdx, 1 
    push 0 
    mov rsi, rsp 
    syscall
    pop rax 
    ret  

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    xor rax, rax
    
    .loop:
        push rcx
        push rsi
        push rdi
        call read_char
        pop rdi
        pop rsi
        pop rcx

        cmp rax, 0 
        je .end

        cmp rax, ' ' 
        je .skip
        cmp rax, '	'
        je .skip
        cmp rax, '\n'
        je .skip

        mov [rdi+rcx], rax 
        inc rcx 
        cmp rcx, rsi 
        jge .over_buffer

        jmp .loop

    .skip:
        cmp rcx,0 
        je .loop
        jmp .end 
    .over_buffer:                                                                
        xor rax, rax
        xor rdx, rdx
        ret
    .end:
        xor rax, rax
        mov [rdi+rcx], rax 
        mov rax, rdi
        mov rdx, rcx 
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10 
    .loop: 
        movzx r9, byte[rdi+rcx] 
        cmp r9,0 
        je .end
        cmp r9b, '0' 
        jl .end     
        cmp r9b, '9'
        jg .end

        mul r8 
        sub r9b, '0' 
        add rax, r9 
        inc rcx 
        jmp .loop

    .end:
        mov rdx, rcx 
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx 
    
    cmp byte[rdi], '-'
    je .negative
    jmp parse_uint
    .negative:
        inc rdi 
        
        push rdi
        call parse_uint 
        pop rdi
        neg rax 
        inc rdx 
        ret
 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
     
    push rsi
    push rdi
    push rdx
    call string_length
    pop rdx
    pop rdi
    pop rsi
    xor rcx, rcx
    
    inc rax
    cmp rdx, rax 
    
    jl .error
    .loop:
        cmp rcx, rax
        jg .end
        mov r10b,[rdi+rcx] 
        mov [rsi+rcx], r10b
        inc rcx
        jmp .loop
    
    .error:
        xor rax, rax 
        ret
    .end:
     
        ret
